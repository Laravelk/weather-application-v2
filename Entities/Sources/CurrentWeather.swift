//
//  CurrentWeather.swift
//  Entities
//
//  Created by Jakob Vinther-Larsen on 19/02/2019.
//  Copyright © 2019 SHAPE A/S. All rights reserved.
//

import Foundation

public struct CurrentWeather: Codable {
    enum CodingKeys: String, CodingKey {
        case weather
        case main
        case wind
        case date = "dt"
        case name
    }
    
    public let weather: [WeatherInfo]
    public let main: MainWeatherInfo
    public let wind: WindInfo
    public let date: Int
    public let name: String
}
