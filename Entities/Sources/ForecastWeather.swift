//
//  ForecastWeather.swift
//  Entities
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

public struct MainWeatherInfo: Codable {
    enum CodingKeys: String, CodingKey {
        case tempurature = "temp"
        case tempuratureFeelsLike = "feels_like"
        case minTempurature = "temp_min"
        case maxTempurature = "temp_max"
        case pressure = "pressure"
        case humidity = "humidity"
    }
    
    public let tempurature: Double
    public let tempuratureFeelsLike: Double
    public let minTempurature: Double
    public let maxTempurature: Double
    public let pressure: Double
    public let humidity: Double
}

public struct WeatherInfo: Codable {
    public enum WeatherCondition: String, Codable {
        case thunderstorm = "Thunderstorm"
        case clear = "Clear"
        case drizzle = "Drizzle"
        case rain = "Rain"
        case snow = "Snow"
        case mist = "Mist"
        case smoke = "Smoke"
        case haze = "Haze"
        case dust = "Dust"
        case fog = "Fog"
        case sand = "Sand"
        case ash = "Ash"
        case squall = "Squall"
        case tornado = "Tornado"
        case clouds = "Clouds"
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case main
        case description
        case icon
    }
    
    public let id: Int
    public let main: WeatherCondition
    public let description: String
    public let icon: String
}

public struct WindInfo: Codable {
    enum CodingKeys: String, CodingKey {
        case speed
        case degrees = "deg"
        case gust = "gust"
    }
    
    public let speed: Double
    public let degrees: Int
    public let gust: Double
}

public struct HourlyWeather: Codable {
    enum CodingKeys: String, CodingKey {
        case date = "dt"
        case main
        case weather
        case wind
        case visibility
    }
    
    public let date: Int
    public let main: MainWeatherInfo
    public let weather: [WeatherInfo]
    public let wind: WindInfo
    public let visibility: Double
}

public struct CityInfo: Codable {
    public struct Coordinate: Codable {
        enum CodingKeys: String, CodingKey {
            case latitude = "lat"
            case longitude = "lon"
        }
        
        public let latitude: Double
        public let longitude: Double
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case coordinate = "coord"
        case country
        case sunrise
        case sunset
    }
    
    public let id: Int
    public let name: String
    public let coordinate: Coordinate
    public let country: String
    public let sunrise: Int
    public let sunset: Int
}

public struct ForecastWeather: Codable {
    enum CodingKeys: String, CodingKey {
        case count = "cnt"
        case weathers = "list"
        case city
    }
    
    public let count: Int
    public let weathers: [HourlyWeather]
    public let city: CityInfo
}
