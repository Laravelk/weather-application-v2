//
//  Weather.swift
//  API
//
//  Created by Jakob Vinther-Larsen on 19/02/2019.
//  Copyright © 2019 SHAPE A/S. All rights reserved.
//

import Entities
import Client
import CoreLocation
import UIKit

extension CurrentWeather {
    public static func getIconImageEndPoint(for id: String) -> String {
        "https://openweathermap.org/img/wn/\(id).png"
    }
    
    public static func getCurrent(for query: String) -> Request<CurrentWeather, APIError> {
        Request(
            url: URL(string: "weather")!,
            method: .get,
            parameters: [QueryParameters([URLQueryItem(name: "q", value: query)])],
            resource: decodeResource(CurrentWeather.self),
            error: APIError.init,
            needsAuthorization: true
        )
    }
    
    public static func getCurrent(for coordinate: CLLocationCoordinate2D) -> Request<CurrentWeather, APIError> {
        Request(
            url: URL(string: "weather")!,
            method: .get,
            parameters: [QueryParameters([
                URLQueryItem(name: "lat", value: String(coordinate.latitude)),
                URLQueryItem(name: "lon", value: String(coordinate.longitude)),
                URLQueryItem(name: "units", value: "metric")
            ])],
            resource: decodeResource(CurrentWeather.self),
            error: APIError.init,
            needsAuthorization: true
        )
    }
    
    public static func getForecast(for coordinate: CLLocationCoordinate2D) -> Request<ForecastWeather, APIError> {
        .init(
            url: URL(string: "forecast")!,
            method: .get,
            parameters: [QueryParameters([
                URLQueryItem(name: "lat", value: String(coordinate.latitude)),
                URLQueryItem(name: "lon", value: String(coordinate.longitude)),
                URLQueryItem(name: "units", value: "metric")
            ])],
            resource: decodeResource(ForecastWeather.self),
            error: APIError.init,
            needsAuthorization: true
        )
    }
}
