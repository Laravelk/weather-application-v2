//
//  LaunchCoordinator.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit
import API

final class LaunchCoordinator {
    private weak var navigationController: UINavigationController?
    private weak var windowNavigation: SingleNavigationControllerType?
    
    private var findLocationCordinator: FindLocationCoordinator?
    
    init(windowNavigation: SingleNavigationControllerType) {
        self.windowNavigation = windowNavigation
        startController()
    }
    
    func startController() {
        let navigationController = prepareNavigationController()
        let client = ForecastClient(appId: "f1d25bbb5cc19bfcf8d9c9e4c1bc9f86")
        findLocationCordinator = FindLocationCoordinator(
            navigationController: navigationController,
            configuration: .init(apiClient: client)
        )
        
        guard let findLocationCordinator = findLocationCordinator else {
            return
        }
        
        let controller = findLocationCordinator.makeInitial()
        navigationController.viewControllers = [controller]
        windowNavigation?.push(navigationController)
    }
    
    private func prepareNavigationController() -> UINavigationController {
        let nc = UINavigationController()
        nc.setNavigationBarHidden(true, animated: false)
        
        self.navigationController = nc
        return nc
    }
}
