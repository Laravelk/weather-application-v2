//
//  DateFormatter.swift
//  forecast
//
//  Created by Иван Морозов on 27.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import Foundation

extension DateFormatter {
    static let onlyTimeFormatteer: DateFormatter = { formatter in
        formatter.dateFormat = "HH:mm"
        return formatter
    }(DateFormatter())
}
