//
//  CoordinatorType.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit

protocol CoordinatorType {
    /// Concrete type for initial view controller
    associatedtype InitialViewController: UIViewController = UIViewController
    
    /// Make initial view controller for a section.
    func makeInitial() -> InitialViewController
}
