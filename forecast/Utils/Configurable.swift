//
//  Configurable.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

protocol Configurable: AnyObject {
    associatedtype Model
    
    func configure(with model: Model)
}
