//
//  ThemeApplicable.swift
//  forecast
//
//  Created by Иван Морозов on 28.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit

protocol ThemeApplicable {
    func applyTheme(_ theme: Theme)
}

struct ThemeViewModel {
    let backgroundColor: UIColor
    let startGradientColor: UIColor
    let endGradientColor: UIColor
}

enum Theme {
    case `default`
    case thunderstorm
    case test
    
    var backgroundColor: UIColor {
        switch self {
        case .thunderstorm: return UIColor(hex: "#0b2a6b", alpha: 0.5)
        case .test: return Colors.primaryBackgroundColor
        default: return UIColor(hex: "#0b2a6b", alpha: 0.5)
        }
    }
    
    var startGradientColor: UIColor {
        switch self {
        case .thunderstorm: return UIColor(hex: "#0b0538")
        case .test: return .white
        default: return UIColor(hex: "#0b0538")
        }
    }
    
    var endGradientColor: UIColor {
        switch self {
        case .thunderstorm: return UIColor(hex: "#0b78ef")
        case .test: return .white
        default: return UIColor(hex: "#0b78ef")
        }
    }
    
    var primaryText: UIColor {
        switch self {
        case .thunderstorm: return .white
        case .test: return .black
        default: return .white
        }
    }
}

extension WeatherCondition {
    var theme: Theme {
        switch self {
        case .thunderstorm: return .thunderstorm
        default: return .`default`
        }
    }
}
