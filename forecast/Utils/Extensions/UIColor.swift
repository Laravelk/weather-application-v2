//
//  UIColor.swift
//  forecast
//
//  Created by Иван Морозов on 27.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(hex: String, alpha: CGFloat = 1) {
        self.init(hex: UInt(hex.dropFirst(), radix: 16) ?? 0, alpha: alpha)
    }
    
    convenience init(hex: UInt, alpha: CGFloat = 1) {
        self.init(
            red: .init((hex & 0xff0000) >> 16) / 255,
            green: .init((hex & 0xff00) >> 8) / 255,
            blue:  .init( hex & 0xff) / 255,
            alpha: alpha
        )
    }
}
