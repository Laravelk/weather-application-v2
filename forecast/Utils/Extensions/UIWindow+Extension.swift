//
//  UIWindow+Extension.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit

protocol SingleNavigationControllerType: AnyObject {
    func push(_ item: UIViewController)
}

// MARK: - SingleNavigationControllerType
 
extension UIWindow: SingleNavigationControllerType {
    func push(_ item: UIViewController) {
        set(rootViewController: item)
    }
}

private extension UIWindow {
    func set(rootViewController newRootViewController: UIViewController) {
            let previousViewConroller = rootViewController
            
            rootViewController = newRootViewController
            
            if let previousViewConroller = previousViewConroller {
                previousViewConroller.dismiss(animated: false) {
                    previousViewConroller.view.removeFromSuperview()
                }
            }
        }

}
