//
//  UIImageView.swift
//  forecast
//
//  Created by Иван Морозов on 24.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
