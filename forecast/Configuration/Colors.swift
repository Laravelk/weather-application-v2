//
//  Colors.swift
//  forecast
//
//  Created by Иван Морозов on 27.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit

enum Colors {
    static let primaryBackgroundColor = UIColor(hex: "#F9F9F9")
}
