
//
//  FindLocationRouter.swift
//  forecast
//
//  Created by Jakob Vinther-Larsen on 19/02/2019.
//  Copyright © 2019 SHAPE A/S. All rights reserved.
//

import MapKit
import API

protocol FindLocationRouterEventDelegate: AnyObject {
    func routeToSelectedLocation(_: CLLocationCoordinate2D)
}

final class FindLocationRouter {
    private let api: ForecastClient
    
    weak var delegate: FindLocationRouterEventDelegate?
    
    init(api apiClient: ForecastClient) {
        self.api = apiClient
    }
}

// MARK: - FindLocationInteractorAction

extension FindLocationRouter: FindLocationInteractorAction {
    func locationSelected(at coordinate: CLLocationCoordinate2D) {
        guard let delegate = delegate else { return }
        
        delegate.routeToSelectedLocation(coordinate)
    }
}
