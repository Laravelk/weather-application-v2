//
//  FindLocationCoordinator.swift
//  forecast
//
//  Created by Jakob Vinther-Larsen on 19/02/2019.
//  Copyright © 2019 SHAPE A/S. All rights reserved.
//

import UIKit
import CoreLocation
import API

final class FindLocationCoordinator: CoordinatorType {
    struct Configuration {
        let apiClient: ForecastClient
    }
    
    private let navigationController: UINavigationController
    private let configuration: Configuration
    
    init(navigationController: UINavigationController, configuration: Configuration) {
        self.navigationController = navigationController
        self.configuration = configuration
    }
    
    func makeInitial() -> UIViewController {
        let viewController = FindLocationViewController()
        let interactor = FindLocationInteractor(api: configuration.apiClient)
        let presenter = FindLocationPresenter()
        let router = FindLocationRouter(api: configuration.apiClient)
        
        viewController.delegate = interactor
        interactor.action = router
        interactor.output = presenter
        presenter.output = viewController
        
        router.delegate = self
        
        return viewController
    }
}

// MARK: -

extension FindLocationCoordinator: FindLocationRouterEventDelegate {
    func routeToSelectedLocation(_ coordinate: CLLocationCoordinate2D) {
        let coordinator =
        LocationWeatherCoordinator(
            configuration: .init(
                coordinate: coordinate,
                api: configuration.apiClient
            ),
            navigationController: navigationController
        )
        
        let viewController = coordinator.makeInitial()
        navigationController.pushViewController(viewController, animated: true)
    }
}
