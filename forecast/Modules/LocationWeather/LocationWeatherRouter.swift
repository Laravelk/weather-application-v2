//
//  LocationWeatherRouter.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import Foundation
import UIKit

protocol LocationWeatherRouterType: AnyObject {
    func returnToMap()
}

final class LocationWeatherRouter {
    private let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
}

// MARK: - LocationWeatherRouterType

extension LocationWeatherRouter: LocationWeatherRouterType {
    func returnToMap() {
        navigationController.popViewController(animated: true)
    }
}
