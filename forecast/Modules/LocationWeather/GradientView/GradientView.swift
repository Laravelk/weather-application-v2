//
//  GradientView.swift
//  forecast
//
//  Created by Иван Морозов on 28.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit
import Entities

typealias WeatherCondition = Entities.WeatherInfo.WeatherCondition

final class GradienView: UIView {
    var startColor: UIColor?
    var endColor: UIColor?
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        guard let startColor = startColor, let endColor = endColor else {
            return
        }

        (layer as! CAGradientLayer).colors = [startColor.cgColor, endColor.cgColor]
    }
}

// MARK: - ThemeApplicable

extension GradienView: ThemeApplicable {
    func applyTheme(_ theme: Theme) {
        startColor = theme.startGradientColor
        endColor = theme.endGradientColor
        
        layoutSubviews()
    }
}
