//
//  LocationWeatherInteractor.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import Foundation
import CoreLocation

import API
import Entities

public typealias ForecastWeather = Entities.ForecastWeather
public typealias CurrentWeather = Entities.CurrentWeather

protocol LocationWeatherInteractorType: AnyObject {
    func getForecastWeather(for position: CLLocationCoordinate2D)
    func getCurrentWeather(for position: CLLocationCoordinate2D)
}

final class LocationWeatherInteractor {
    weak var presenter: LocationWeatherPresenterType?
    
    private let api: ForecastClient
        
    init(api apiClient: ForecastClient) {
        self.api = apiClient
    }
}

// MARK: - LocationWeatherInteractorType

extension LocationWeatherInteractor: LocationWeatherInteractorType {
    func getForecastWeather(for position: CLLocationCoordinate2D) {
        _ = api.perform(CurrentWeather.getForecast(for: position)) { [weak self] response in
            switch response {
            case .success(let weather):
                self?.presenter?.didLoadForecastWeather(with: weather)
            case .failure:
                self?.presenter?.didLoadForecastWeather(with: nil)
            }
        }
    }
    
    func getCurrentWeather(for position: CLLocationCoordinate2D) {
        _ = api.perform(CurrentWeather.getCurrent(for: position)) { [weak self] response in
            switch response {
            case .success(let weather):
                self?.presenter?.didLoadCurrentWeather(with: weather)
            case .failure:
                self?.presenter?.didLoadCurrentWeather(with: nil)
            }
        }
    }
}
 
