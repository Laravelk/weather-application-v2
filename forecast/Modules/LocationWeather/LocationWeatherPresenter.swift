//
//  LocationWeatherPresenter.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationWeatherPresenterType: AnyObject {
    func didLoadForecastWeather(with weather: ForecastWeather?)
    func didLoadCurrentWeather(with weather: CurrentWeather?)
    func didLoadView()
    
    func retryRequests()
    func returnToMap()
}

final class LocationWeatherPresenter {
    struct LocationWeatherConfiguration {
        let coordinate: CLLocationCoordinate2D
    }
    
    weak var viewController: LocationWeatherViewControllerType?
    
    private let configuration: LocationWeatherConfiguration
    private let interactor: LocationWeatherInteractorType
    private let router: LocationWeatherRouterType
    
    init(
        configuration: LocationWeatherConfiguration,
        interactor: LocationWeatherInteractorType,
        router: LocationWeatherRouterType
    ) {
        self.configuration = configuration
        self.interactor = interactor
        self.router = router
    }
}

// MARK: - LocationWeatherPresenterType
 
extension LocationWeatherPresenter: LocationWeatherPresenterType {
    func didLoadForecastWeather(with weather: ForecastWeather?) {
        if let weather = weather {
            viewController?.updateForecast(with: weather)
        } else {
            viewController?.showAlert(message: "Forecast weather not available")
        }
    }
    
    func didLoadCurrentWeather(with weather: CurrentWeather?) {
        if let weather = weather {
            let additionalInfo: [AdditionalInfoCellModel] = [
                .init(
                    type: .feelsLike,
                    value: "\(Int(weather.main.tempuratureFeelsLike))"
                ),
                .init(
                    type: .windSpeed,
                    value: "\(Int(weather.wind.speed))"
                ),
                .init(
                    type: .drop,
                    value: "\(Int(weather.main.humidity))"
                ),
                .init(
                    type: .pressure,
                    value: "\(Int(weather.main.pressure))"
                )
            ]
            
            viewController?.updateAdditionalInfo(with: additionalInfo)
            viewController?.updateCurrent(with: weather)
        } else {
            viewController?.showAlert(message: "Current weather not available")
        }
    }
    
    func didLoadView() {
        makeRequests()
    }
    
    func retryRequests() {
        makeRequests()
    }
    
    func returnToMap() {
        router.returnToMap()
    }
}

private extension LocationWeatherPresenter {
    func makeRequests() {
        interactor.getCurrentWeather(for: configuration.coordinate)
        interactor.getForecastWeather(for: configuration.coordinate)
    }
}
