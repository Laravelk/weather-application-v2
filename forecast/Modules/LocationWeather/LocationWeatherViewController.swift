//
//  LocationWeatherViewController.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit

protocol LocationWeatherViewControllerType: AnyObject {
    func updateForecast(with weather: ForecastWeather)
    func updateCurrent(with weather: CurrentWeather)
    func updateAdditionalInfo(with info: [AdditionalInfoCellModel])
    func showAlert(message: String)
}

final class LocationWeatherViewController: UIViewController {
    private let tableView = UITableView(frame: .zero, style: .grouped)
    private let activityIndicator = UIActivityIndicatorView()
    private let gradientView = GradienView()
    
    var presenter: LocationWeatherPresenterType?
    
    private var forecastWeather: ForecastWeather?
    private var currentWeather: CurrentWeather?
    private var additionalInfo: [AdditionalInfoCellModel] = []
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        configureViewController()
        
        activityIndicator.startAnimating()
        presenter?.didLoadView()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension LocationWeatherViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        section == 2 ? additionalInfo.count : 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == .zero {
            return .leastNonzeroMagnitude
        }
        
        return tableView.sectionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 380
        } else if indexPath.section == 1 {
            return 128
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let currentWeather = currentWeather,
              let forecastWeather = forecastWeather,
              let theme = currentWeather.weather.first?.main.theme
        else { return UITableViewCell() }
        
        if indexPath.section == .zero {
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: MainTableViewCell.identifier,
                for: indexPath
            ) as? MainTableViewCell else {
                return UITableViewCell()
            }
            
             cell.configure(with: .init(
                cityName: forecastWeather.city.name,
                currentWeather: currentWeather
            ))
            cell.applyTheme(theme)
            
            return cell
        } else if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: HourlyTableViewCell.identifier,
                for: indexPath
            ) as? HourlyTableViewCell
            else {
                return UITableViewCell()
            }
            
            cell.configure(with: .init(forecastWeather: forecastWeather))
            cell.applyTheme(theme)
            
            return cell
        } else if indexPath.section == 2 {
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: AdditionalInfoCell.identifier,
                for: indexPath
            ) as? AdditionalInfoCell else {
                return UITableViewCell()
            }
            
            cell.configure(with: additionalInfo[indexPath.item])
            cell.applyTheme(theme)
            
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                cell.makeRoundCorners(type: .top)
            } else if indexPath.row == .zero {
                cell.makeRoundCorners(type: .bottom)
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
}

// MARK: - LocationWeatherViewControllerType

extension LocationWeatherViewController: LocationWeatherViewControllerType {
    func updateForecast(with weather: ForecastWeather) {
        forecastWeather = weather
        updateTable()
    }
    
    func updateCurrent(with weather: CurrentWeather) {
        currentWeather = weather
        updateTable()
    }
    
    func updateAdditionalInfo(with info: [AdditionalInfoCellModel]) {
        self.additionalInfo = info
        updateTable()
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(
            title: "Error",
            message: message,
            preferredStyle: .alert
        )
        
        let retryAction = UIAlertAction(title: "Retry", style: .default) { [weak self] _ in
            self?.presenter?.retryRequests()
        }
        
        let returnToMap = UIAlertAction(title: "Return to map", style: .cancel) { [weak self] _ in
            self?.presenter?.returnToMap()
        }
        
        alert.addAction(retryAction)
        alert.addAction(returnToMap)
        
        present(alert, animated: true, completion: nil)
    }
    
    private func makeGradient() {
        guard let condition = currentWeather?.weather.first?.main else { return }
        
        gradientView.applyTheme(condition.theme)
    }
    
    private func updateTable() {
        guard forecastWeather != nil, currentWeather != nil else {
            return
        }
                
        DispatchQueue.main.async { [weak self] in
            self?.makeGradient()
            self?.activityIndicator.stopAnimating()
            self?.tableView.reloadData()
        }
    }
}

// MARK: - Configure View

private extension LocationWeatherViewController {
    func configureViewController() {
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .lightGray
        }
        
        configureGradientView()
        configureTable()
        configureActivityIndicator()
    }
    
    func configureGradientView() {
        view.addSubview(gradientView)
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            gradientView.topAnchor.constraint(equalTo: view.topAnchor),
            gradientView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            gradientView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            gradientView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    func configureActivityIndicator() {
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
    }
    
    func configureTable() {
        gradientView.addSubview(tableView)
        
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = false
        tableView.alwaysBounceVertical = false
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: gradientView.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: gradientView.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: gradientView.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: gradientView.trailingAnchor)
        ])
            
        tableView.register(MainTableViewCell.self, forCellReuseIdentifier: MainTableViewCell.identifier)
        tableView.register(HourlyTableViewCell.self, forCellReuseIdentifier: HourlyTableViewCell.identifier)
        tableView.register(AdditionalInfoCell.self, forCellReuseIdentifier: AdditionalInfoCell.identifier)
    }
}
