//
//  LocationWeatherCoordinator.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit
import CoreLocation
import API

final class LocationWeatherCoordinator: CoordinatorType {
    struct Configuration {
        let coordinate: CLLocationCoordinate2D
        let api: ForecastClient
    }
    
    private let configuration: Configuration
    private let navigationController: UINavigationController
    
    init(configuration: Configuration, navigationController: UINavigationController) {
        self.configuration = configuration
        self.navigationController = navigationController
    }
    
    func makeInitial() -> UIViewController {
        let viewController = LocationWeatherViewController()
        let interactor = LocationWeatherInteractor(api: configuration.api)
        let router = LocationWeatherRouter(navigationController: navigationController)
        let presenter = LocationWeatherPresenter(
            configuration: .init(coordinate: configuration.coordinate),
            interactor: interactor,
            router: router
        )
        
        presenter.viewController = viewController
        viewController.presenter = presenter
        interactor.presenter = presenter
        
        return viewController
    }
}
