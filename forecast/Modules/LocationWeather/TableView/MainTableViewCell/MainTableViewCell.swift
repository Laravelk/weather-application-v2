//
//  MainTableViewCell.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit

private struct Constants {
    let topOffset: CGFloat = 12.0
    let cityNameTextSize: CGFloat = 40
    let descriptionTextSize: CGFloat = 22
    let temperatureTextSize: CGFloat = 90
    let imageWidth: CGFloat = 120.0
    let stackSpacing = 10.0
}
private let constants = Constants()

struct MainTableViewCellModel {
    let cityName: String
    let currentWeather: CurrentWeather
}

final class MainTableViewCell: UITableViewCell {
    private let stack = UIStackView()
    private let imageStack = UIStackView()
    
    private let cityName = UILabel()
    private let descriptionWeather = UILabel()
    private let currentTempurature = UILabel()
    private let minMaxTempurature = UILabel()
    private let conditionIcon = UIImageView()
    
    static let identifier = "MainTableViewCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Configurable

extension MainTableViewCell: Configurable {
    typealias Model = MainTableViewCellModel
    
    func configure(with model: MainTableViewCellModel) {
        let city = !model.cityName.isEmpty ? model.cityName : "Unknown"
        
        cityName.attributedText = NSAttributedString(
            string: city,
            attributes: [.font: UIFont.boldSystemFont(ofSize: constants.cityNameTextSize)]
        )
        descriptionWeather.attributedText = NSAttributedString(
            string: model.currentWeather.weather.first?.description ?? "",
            attributes: [.font: UIFont.systemFont(ofSize: constants.descriptionTextSize)]
        )
        currentTempurature.attributedText = NSAttributedString(
            string: String(Int(model.currentWeather.main.tempurature)) + "°C",
            attributes: [.font: UIFont.systemFont(ofSize: constants.temperatureTextSize)]
        )
        minMaxTempurature.text = "Max " + String(Int(model.currentWeather.main.minTempurature)) + "°C, Min " + String(Int(model.currentWeather.main.maxTempurature)) + "°C"
        
        guard let weather = model.currentWeather.weather.first else { return}
        
        if #available(iOS 13.0, *), !ApplicationConstants.isLoadImages {
            conditionIcon.image = UIImage(systemName: weather.getSystemIconByWeatherId())
        } else {
            let imageEndPoint = CurrentWeather.getIconImageEndPoint(for: weather.icon)
            conditionIcon.load(url: URL(string: imageEndPoint)!)
        }
    }
}

// MARK: - ThemeApplicable

extension MainTableViewCell: ThemeApplicable {
    func applyTheme(_ theme: Theme) {
        cityName.textColor = theme.primaryText
        descriptionWeather.textColor = theme.primaryText
        currentTempurature.textColor = theme.primaryText
        minMaxTempurature.textColor = theme.primaryText
    }
}

// MARK: - InitView

private extension MainTableViewCell {
    func configureView() {
        configureConditionIcon()
        configureStack()
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    func configureConditionIcon() {
        imageStack.addArrangedSubview(conditionIcon)
        
        imageStack.distribution = .fill
        imageStack.contentMode = .scaleToFill
        
        NSLayoutConstraint.activate([
            conditionIcon.widthAnchor.constraint(equalToConstant: constants.imageWidth),
            conditionIcon.heightAnchor.constraint(equalTo: conditionIcon.widthAnchor),
            imageStack.widthAnchor.constraint(equalToConstant: constants.imageWidth),
            imageStack.heightAnchor.constraint(equalTo: imageStack.widthAnchor)
        ])
    }
    
    func configureStack() {
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(stack)
        
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: constants.topOffset),
            stack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            stack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
                
        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .fill
        stack.contentMode = .scaleAspectFill
        stack.spacing = constants.stackSpacing
        
        stack.addArrangedSubview(cityName)
        stack.addArrangedSubview(descriptionWeather)
        stack.addArrangedSubview(currentTempurature)
        stack.addArrangedSubview(minMaxTempurature)
        stack.addArrangedSubview(imageStack)
    }
}
