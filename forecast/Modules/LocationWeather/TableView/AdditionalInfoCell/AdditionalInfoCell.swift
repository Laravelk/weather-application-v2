//
//  AdditionalInfoCell.swift
//  forecast
//
//  Created by Иван Морозов on 27.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit

private struct Constants {
    let containerLeadingOffset = 20.0
    let containerTrailingOffset = -20.0
    
    let iconWidth = 20.0
    let iconLeadingOffset = 10.0
    let iconeTopOffset = 10.0
    
    let titleLeadingOffset = 10.0
    
    let subtitleLeadingOffset = 10.0
    let subtitleTrailingOffset = -40.0
    
    let cornerRadius = 12.0
    let separatorViewHeight = 2.0
}
private let constants = Constants()

enum AdditionalInfoCellType {
    case feelsLike
    case windSpeed
    case drop
    case pressure
    
    var title: String {
        switch self {
        case .feelsLike: return "Feels like"
        case .windSpeed: return "Wind Speed"
        case .drop: return "Humidity"
        case .pressure: return "Pressure"
        }
    }
    
    var icon: String {
        switch self {
        case .feelsLike:
            return "staroflife"
        case .windSpeed:
            return "wind"
        case .drop:
            return "drop"
        case .pressure:
            return "thermometer"
        }
    }
}

struct AdditionalInfoCellModel {
    let type: AdditionalInfoCellType
    let value: String
}

final class AdditionalInfoCell: UITableViewCell {
    enum CornerRadiusType {
        case top
        case bottom
    }
    
    static let identifier = "AdditionalInfoCell"
    
    private let containerView = UIView()
    private let iconView = UIImageView()
    private let title = UILabel()
    private let subtitle = UILabel()
    
    private var theme: Theme = .default
            
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        containerView.layer.maskedCorners = []
        containerView.layer.cornerRadius = .zero
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Round corners

extension AdditionalInfoCell {
    func makeRoundCorners(
        type: CornerRadiusType,
        with cornerRadius: Double = constants.cornerRadius
    ) {
        switch type {
        case .top:
            containerView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        case .bottom:
            containerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        
        containerView.layer.cornerRadius = cornerRadius
    }
}

// MARK: - Configurable

extension AdditionalInfoCell: Configurable {
    typealias Model = AdditionalInfoCellModel
    
    func configure(with model: AdditionalInfoCellModel) {
        title.text = model.type.title
        subtitle.text = model.value
        
        if #available(iOS 13.0, *) {
            iconView.image = UIImage(systemName: model.type.icon)
        } else {
            iconView.image = UIImage(named: "weather_default")
        }
    }
}

// MARK: - ThemeApplicable

extension AdditionalInfoCell: ThemeApplicable {
    func applyTheme(_ theme: Theme) {
        containerView.backgroundColor = theme.backgroundColor
        title.textColor = theme.primaryText
        subtitle.textColor = theme.primaryText
    }
}

// MARK: - InitView

private extension AdditionalInfoCell {
    func configureView() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        contentView.clipsToBounds = true
        
        configureContainerView()
        configureIcon()
        configureTitle()
        configureSubtitle()
        addSeparatorView()
    }
    
    func addSeparatorView() {
        let separatorView = UIView()
        separatorView.backgroundColor = .clear
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(separatorView)
        NSLayoutConstraint.activate([
            separatorView.topAnchor.constraint(equalTo: containerView.bottomAnchor),
            separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: constants.separatorViewHeight),
            separatorView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)
        ])
    }
    
    func configureContainerView() {
        contentView.addSubview(containerView)
        containerView.backgroundColor = Colors.primaryBackgroundColor
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.clipsToBounds = true
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor),
            containerView.leadingAnchor.constraint(
                equalTo: contentView.leadingAnchor,
                constant: constants.containerLeadingOffset
            ),
            containerView.trailingAnchor.constraint(
                equalTo: contentView.trailingAnchor,
                constant: constants.containerTrailingOffset
            )
        ])
    }
    
    func configureSubtitle() {
        containerView.addSubview(subtitle)
        subtitle.translatesAutoresizingMaskIntoConstraints = false
        subtitle.clipsToBounds = true
        
        NSLayoutConstraint.activate([
            subtitle.centerYAnchor.constraint(equalTo: iconView.centerYAnchor),
            subtitle.trailingAnchor.constraint(
                equalTo: containerView.trailingAnchor,
                constant: constants.subtitleTrailingOffset
            ),
            subtitle.leadingAnchor.constraint(equalTo: title.trailingAnchor)
        ])
        
        subtitle.textAlignment = .right
    }
    
    func configureTitle() {
        containerView.addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.clipsToBounds = true
        
        NSLayoutConstraint.activate([
            title.centerYAnchor.constraint(equalTo: iconView.centerYAnchor),
            title.leadingAnchor.constraint(
                equalTo: iconView.trailingAnchor,
                constant: constants.titleLeadingOffset
            )
        ])
        
        title.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        
        title.textAlignment = .left
    }
    
    func configureIcon() {
        containerView.addSubview(iconView)
        iconView.contentMode = .scaleAspectFit

        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.clipsToBounds = true
        
        NSLayoutConstraint.activate([
            iconView.widthAnchor.constraint(equalToConstant: constants.iconWidth),
            iconView.heightAnchor.constraint(equalTo: iconView.widthAnchor),
            iconView.leadingAnchor.constraint(
                equalTo: containerView.leadingAnchor,
                constant: constants.iconLeadingOffset
            ),
            iconView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
        ])
    }
}
