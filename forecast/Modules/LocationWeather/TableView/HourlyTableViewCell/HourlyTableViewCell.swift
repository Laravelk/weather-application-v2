//
//  HourlyTableViewCell.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit

private struct Constants {
    let collectionItemWidth = 75.0
    let collectionItemHeight = 100.0
    
    let collectionTopConstraintToSuperView = 10.0
    let collectionLeadingConstraintToSuperView = 20.0
    let collectionTrailingConstraintToSuperView = -20.0
}
private let constants = Constants()

struct HourlyTableViewCellModel {
    let forecastWeather: ForecastWeather
}

final class HourlyTableViewCell: UITableViewCell {
    private var collectionView: UICollectionView?
    private var forecastWeather: ForecastWeather?
    
    static let identifier = "HourlyTableViewCell"
    private var theme: Theme = .default
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Configurable

extension HourlyTableViewCell: Configurable {
    typealias Model = HourlyTableViewCellModel
    
    func configure(with model: HourlyTableViewCellModel) {
        forecastWeather = model.forecastWeather

        collectionView?.reloadData()
    }
}

extension HourlyTableViewCell: ThemeApplicable {
    func applyTheme(_ theme: Theme) {
        self.theme = theme
        
        collectionView?.backgroundColor = theme.backgroundColor
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout

extension HourlyTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        forecastWeather?.weathers.count ?? .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let forecastWeather = forecastWeather,
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HourCell.identifier, for: indexPath) as? HourCell
        else {
            return UICollectionViewCell()
        }
        
        cell.configure(with: forecastWeather.weathers[indexPath.row])
        cell.applyTheme(forecastWeather.weathers[indexPath.row].weather.first?.main.theme ?? .default)
        return cell
        
    }
}

// MARK: - Configure

private extension HourlyTableViewCell {
    func configureView() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        configureCollectionView()
    }
    
    func configureCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(
            width: constants.collectionItemWidth,
            height: constants.collectionItemHeight
        )
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        
        guard let collectionView = collectionView else { return }
        
        contentView.addSubview(collectionView)
                
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(
                equalTo: contentView.topAnchor,
                constant: constants.collectionTopConstraintToSuperView
            ),
            collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            collectionView.trailingAnchor.constraint(
                equalTo: contentView.trailingAnchor,
                constant: constants.collectionTrailingConstraintToSuperView
            ),
            collectionView.leadingAnchor.constraint(
                equalTo: contentView.leadingAnchor,
                constant: constants.collectionLeadingConstraintToSuperView
            )
        ])
        
        collectionView.allowsSelection = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.layer.cornerRadius = 12.0
        
        collectionView.register(HourCell.self, forCellWithReuseIdentifier: HourCell.identifier)
    }
}
