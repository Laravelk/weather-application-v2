//
//  HourCell.swift
//  forecast
//
//  Created by Иван Морозов on 23.02.2022.
//  Copyright © 2022 SHAPE A/S. All rights reserved.
//

import UIKit
import Entities

private struct Constants {
    let iconHeight = 50.0
}
private let constants = Constants()

final class HourCell: UICollectionViewCell {
    private let stack = UIStackView()
    
    private let time = UILabel()
    private let temperature = UILabel()
    private let icon = UIImageView()
    
    static let identifier = "HourCell"
    
    private var theme: Theme = .default
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Configurable

extension HourCell: Configurable {
    typealias Model = HourlyWeather
    
    func configure(with model: HourlyWeather) {
        let date = Date(timeIntervalSince1970: TimeInterval(model.date))
        time.text = DateFormatter.onlyTimeFormatteer.string(from: date)
        temperature.text = "\(Int(model.main.tempurature))°C"
        
        guard let weather = model.weather.first else { return }
        
        if #available(iOS 13.0, *), !ApplicationConstants.isLoadImages {
            icon.image = UIImage(systemName: weather.getSystemIconByWeatherId())
        } else {
            let imageEndPoint = CurrentWeather.getIconImageEndPoint(for: weather.icon)
            icon.load(url: URL(string: imageEndPoint)!)
        }
    }
}

// MARK: - ThemeApplicable

extension HourCell: ThemeApplicable {
    func applyTheme(_ theme: Theme) {
        self.theme = theme
        
        contentView.backgroundColor = .clear
        time.textColor = theme.primaryText
        temperature.textColor = theme.primaryText
    }
}

// MARK: - Configure View

private extension HourCell {
    func configureView() {
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.distribution = .equalSpacing
        stack.spacing = 1
        
        contentView.addSubview(stack)
        
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: contentView.topAnchor),
            stack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            stack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
        
        stack.axis = .vertical
        
        stack.addArrangedSubview(time)
        stack.addArrangedSubview(icon)
        stack.addArrangedSubview(temperature)
        
        time.textAlignment = .center
        temperature.textAlignment = .center
        icon.contentMode = .center
        
        icon.contentMode = .scaleAspectFit
    }
    
    func configureIcon() {
        icon.contentMode = .scaleAspectFit
        
        NSLayoutConstraint.activate([
            icon.heightAnchor.constraint(equalToConstant: constants.iconHeight),
            icon.widthAnchor.constraint(equalTo: icon.heightAnchor)
        ])
    }
}
